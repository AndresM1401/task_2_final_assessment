# Task 2. Saucedemo Login test cases
 
The purpose of this test cases is to check some situations: empty credentials, missing password and successful login.

## Prerequisites

Before running the tests, make sure you have the following:

* Java Development Kit (JDK) installed
* Selenium WebDriver library 
* Chrome browser (Version 120.0.6099.71).
* Appropriate WebDriver executable (e.g., ChromeDriver) for your browser
### Test Cases:

* UC -1 - Test Login form with empty credentials 1.Type any credentials 2.Clear the inputs. 3.Check the error messages: 3.1 Username is required.

* UC -2 - Test Login form with credentials by passing Username. 1.Type any credentials in user name 2.Enter password and Clear the input. 3.Check the error messages: 3.1 Password is required.

* UC -3 - Test Login form with credentials by passing Username & Password 1.Type credentials in user name which are under Accepted username are sections 2. Enter password as secret sauce. 3. Click on Login and validate the title “Swag Labs” in the dashboard.
## Running the Tests:
* Clone the repository.
* Open the project in your preferred Java IDE.
* Configure the project with the necessary dependencies and WebDriver executable path.
* Run the test classes corresponding to the test scenarios.

