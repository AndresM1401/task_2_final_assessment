package com.epam.automation.andres_mora.Task_2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InventoryPage extends Base{

    By mainTitleLocator = By.xpath("//*[@id=\"header_container\"]/div[1]/div[2]/div");

    public InventoryPage(WebDriver driver) {
        super(driver);
    }

    public String getMainTitle(){
        WebElement mainTitleElement = findElement(mainTitleLocator);
        return getText(mainTitleElement);
    }
}
