package com.epam.automation.andres_mora.Task_2;

import org.openqa.selenium.*;

public class SaucedemoHomePage extends Base{

    By userNameLocator = By.xpath("//*[@id=\"user-name\"]");
    By passwordLocator = By.id("password");
    By loginButtonLocator = By.id("login-button");
    By errorMessageLocator = By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]");

    public SaucedemoHomePage(WebDriver driver) {
        super(driver);
    }

    public void fill(String user, String password){
        WebElement userNameElement = findElement(userNameLocator);
        WebElement passwordElement = findElement(passwordLocator);

        typeText(userNameElement,user);
        typeText(passwordElement,password);

    }
    public void clearName(String user){
        WebElement userNameElement = findElement(userNameLocator);

        for (int i=0;i<user.length(); i++) {
            sendKey(userNameElement,Keys.BACK_SPACE);
        }
    }
    public void clearPassword(String password){
        WebElement passwordElement = findElement(passwordLocator);
        for (int i=0;i<password.length(); i++) {
            sendKey(passwordElement,Keys.BACK_SPACE);
        }
    }
    public void submit(){
        WebElement loginButtonElement = findElement(loginButtonLocator);
        loginButtonElement.click();
    }
    public String getErrorMessage(){
        WebElement errorMessageElement= findElement(errorMessageLocator);
        return errorMessageElement.getText();
    }
}
