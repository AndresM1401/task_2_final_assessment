package com.epam.automation.andres_mora.Task_2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestSaucedemoPage {
    WebDriver driver;
    SaucedemoHomePage saucedemoHomePage;
    InventoryPage inventoryPage;


    @Before
    public void setUp() {
        saucedemoHomePage = new SaucedemoHomePage(driver);
        driver = saucedemoHomePage.chromeDriverConnection();
        saucedemoHomePage.visit("https://www.saucedemo.com/");
        inventoryPage = new InventoryPage(driver);
    }

    @Test
    public void testLoginFormWithEmptyCredentials() {
        String userName = "anyUserName";
        String password = "anyPassword";
        saucedemoHomePage.fill(userName,password);
        saucedemoHomePage.clearName(userName);
        saucedemoHomePage.clearPassword(password);
        saucedemoHomePage.submit();
        assertEquals("Epic sadface: Username is required",saucedemoHomePage.getErrorMessage());
    }

    @Test
    public void testLoginFormWithCredentialsByPassingUsername() {
        String userName = "anyUserName";
        String password = "anyPassword";
        saucedemoHomePage.fill(userName,password);
        saucedemoHomePage.clearPassword(password);
        saucedemoHomePage.submit();
        assertEquals("Epic sadface: Password is required",saucedemoHomePage.getErrorMessage());
    }

    @Test
    public void testLoginFormWithCredentialsByPassingUsernameAndPassword() {
        String userName = "standard_user";
        String password = "secret_sauce";
        saucedemoHomePage.fill(userName,password);
        saucedemoHomePage.submit();
        assertEquals("Swag Labs",inventoryPage.getMainTitle());
    }

    @After
    public void tearDown() throws InterruptedException {
        Thread.sleep(3000);
        driver.quit();
    }
}
